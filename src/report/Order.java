package report;

import java.util.Date;
import java.util.List;

public class Order implements Visitable {
    public String number;
    public double price;
    public Date date;
    List<Product> products;
    
    public Order(String number, List<Product> products) {
        this.number = number;
        this.products = products;        
        this.date = new Date();
    }
    
    
    public String getNumber() {
        return this.number;
    }
    public double getPrice() {
        return this.price;
    }
    public Date getDate() {
        return this.date;
    }
    public void addProduct(Product product) {
        this.products.add(product);
        this.price += product.price;
    }
       @Override
    public String toString() {
        String str = "";
        
        str += "Order: " + this.number + ", date: " + this.date + ", price: " + this.price + "\nProducts:";
        for(Product product : this.products)
            str += "\n\t" + product.toString();
        
        return str;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String giveReport() {
        return this.toString();
    }
    
}
