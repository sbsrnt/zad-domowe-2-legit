package report;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

    	Product p1 = new Product("Ultimate Barbie collection of 450 dolls. Ken gratis", 399.99);
        
        ReportMaker report = new ReportMaker();
        report.visit(p1);
        System.out.println(report.report);
        
        List<Product> list = new ArrayList<Product>();
          list.add(p1);
          list.add(new Product("Lego Collector Edition: Star Destroyer", 599.99));
          
        Order o1 = new Order("Order nr 1", list);
        report.visit(o1);
        System.out.println(report.report);
    }
}