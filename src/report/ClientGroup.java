package report;

import java.util.ArrayList;
import java.util.List;

public class ClientGroup implements Visitable {
    public String name;
    List<Client> clients;
    
    public ClientGroup(String name) {
        this.name = name;
        this.clients = new ArrayList<Client>();
    }
    
    public void addClient(Client client) {
        this.clients.add(client);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String giveReport() {
        return this.toString();
    }
    
    @Override 
    public String toString() {
        String str = "";
        
        str += "ClientGroup: " + this.name + ", Clients:\n";
        for(Client client : this.clients)
            str += "\t" + client.toString() + "\n";
        
        return str;
    }
}