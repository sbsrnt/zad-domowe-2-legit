package report;


public class ReportMaker implements Visitor{
    public int clients;
    public int orders;
    public int products;
    public String report;

    public ReportMaker() {
        clients = 0;
        orders = 0;
        products = 0;
    }
    
    
    public int getNumberOfClients() {
        return clients;
    }
    public int getNumberOfOrders() {
       return orders;
    }
    public int getNumberOfProducts() { 
        return products;
    }
    public String getReport() {
        return report;
    }
    
    @Override
    public void visit(Visitable visitable) {
        this.report += "\n" + visitable.giveReport();
        
        switch(visitable.getClass().getSimpleName()) {
            case "Product": this.products++; break;
            case "Order": this.orders++; break;
            case "Client": this.clients++; break;
        }
    }
    
    
}
