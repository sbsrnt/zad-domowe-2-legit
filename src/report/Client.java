package report;


import java.util.ArrayList;
import java.util.List;

public class Client implements Visitable {
    public String number;
    public List<Order> orders;
    
    public Client() { 
        this("");
    }
    public Client(String number) {
        this.number = number;
        this.orders = new ArrayList<Order>();
    }
    
    public String getNumber() { 
        return this.number;
    }
    public void setNumber(String number) {
        this.number = number;
    }
    public void addOrder(Order order) {
        this.orders.add(order);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
    

    @Override
    public String giveReport() {
        return this.toString();
    }
    
    @Override
    public String toString() {
        String str = "";
        
        str += "Client: " + this.number + ", Orders:\n";
        for(Order order : this.orders)
            str += "\t" + order.toString() + "\n";
        
        return str;
    }
}
